//
//  NYSHomeTableVCTest.swift
//  20210417-sivaramjebamani-nycschoolsTests
//
//  Created by Sivaram jebamani on 4/18/21.
//

import XCTest
@testable import _0210417_sivaramjebamani_nycschools

class NYSHomeTableVCTest: XCTestCase {

    var controller: NYSHomeTableVC!
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        guard let controller = UIStoryboard(name: "Main", bundle: Bundle(for: NYSHomeTableVC.self)).instantiateViewController(identifier: "NYSHomeTableVC") as? NYSHomeTableVC else {
            return XCTFail("Could not instantiate ViewController from main storyboard")
        }
        self.controller = controller
        self.controller.loadViewIfNeeded()
        self.controller.viewDidLoad()
        XCTAssertNotNil(controller.tableView,
                        "Controller should have a tableview")
    }
    
    func testTableViewDelegateIsViewController() {
      XCTAssertTrue(controller.tableView.delegate === controller,
                    "Controller should be delegate for the table view")
    }

    func testTableViewDataSourceIsViewController() {
      XCTAssertTrue(controller.tableView.dataSource === controller,
                    "Controller should be delegate for the table view")
    }

    func testNumberOfRow() {
        self.controller.loadSchoolList()
        sleep(1)
        XCTAssert(self.controller.tableView.numberOfRows(inSection: 0) == self.controller.schoolList?.count)
    }
    
    func testCell() {
        self.controller.loadSchoolList()
        sleep(1)
        let cell: NYSSchoolListCell = self.controller.tableView.dataSource?.tableView(self.controller.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as! NYSSchoolListCell
        XCTAssert(cell.titleLabel.text != nil)
        XCTAssert(cell.subTitleLabel.text != nil)
    }

}
