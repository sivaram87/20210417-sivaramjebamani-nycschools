//
//  _0210417_sivaramjebamani_nycschoolsTests.swift
//  20210417-sivaramjebamani-nycschoolsTests
//
//  Created by Sivaram jebamani on 4/17/21.
//

import XCTest
@testable import _0210417_sivaramjebamani_nycschools

class _0210417_sivaramjebamani_nycschoolsTests: XCTestCase {

    let schoolDataManager = SchoolDataManager()
    
    func testSchoolDataLoading() {
        let expectation = self.expectation(description: "Test School data fetch")
        schoolDataManager.getAllHighSchools { (list, error) in
            XCTAssert(((list?.count ?? 0) > 0))
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10)
    }

    func testSATDataLoading() {
        let expectation = self.expectation(description: "Test School data fetch")
        schoolDataManager.getSATInfo(for: "01M450", { (result, err) in
            XCTAssert(result != nil)
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 10)
    }
}
