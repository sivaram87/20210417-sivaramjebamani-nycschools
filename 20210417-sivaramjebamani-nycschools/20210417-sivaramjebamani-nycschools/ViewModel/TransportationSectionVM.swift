//
//  TransportationSection.swift
//  20210417-sivaramjebamani-nycschools
//
//  Created by Sivaram jebamani on 4/17/21.
//

import UIKit

/**
 This section is reponsible for displaying transportation information such as Bus & Subway details
 */

struct TransportationSection: SectionDetails {
    var title: String = ""
    var cells: [CellType] = []

    init(school: School) {
        self.title = Constants.transportation
        self.cells = [
            CellType.subTitle(title: Constants.bus, subTitle: school.bus ?? ""),
            CellType.subTitle(title: Constants.subway, subTitle: school.subway ?? ""),
                ]
    }
    
    func trigerAction(forIndex index: Int, viewController: UIViewController) {}
}
