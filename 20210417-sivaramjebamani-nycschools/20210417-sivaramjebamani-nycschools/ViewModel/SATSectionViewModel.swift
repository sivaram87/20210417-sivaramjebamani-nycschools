//
//  SATSectionViewModel.swift
//  20210417-sivaramjebamani-nycschools
//
//  Created by Sivaram jebamani on 4/17/21.
//

import UIKit

/**
 This section is reponsible for displaying SAT Results such as,
 * Test Takers
 * Maths Average Score
 * Writing Average Score
 * Reading Average Score
 */
struct SATSection: SectionDetails {
    var title: String = ""
    var cells: [CellType] = []
    
    init(satResult: SATResult) {
        self.title = Constants.satResults
        self.cells = [
            CellType.rightSubTitle(title: Constants.testTaker, subTitle: satResult.num_of_sat_test_takers ?? Constants.na),
            CellType.rightSubTitle(title: Constants.mathScore, subTitle: satResult.sat_math_avg_score ?? Constants.na),
            CellType.rightSubTitle(title: Constants.writingScore, subTitle: satResult.sat_writing_avg_score ?? Constants.na),
            CellType.rightSubTitle(title: Constants.readingScore, subTitle: satResult.sat_critical_reading_avg_score ?? Constants.na),
                ]
    }
    
    func trigerAction(forIndex index: Int, viewController: UIViewController) {}
}
