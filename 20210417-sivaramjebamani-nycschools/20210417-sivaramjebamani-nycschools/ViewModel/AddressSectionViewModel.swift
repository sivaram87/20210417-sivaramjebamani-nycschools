//
//  AccountSectionViewModel.swift
//  20210417-sivaramjebamani-nycschools
//
//  Created by Sivaram jebamani on 4/17/21.
//

import UIKit
import MessageUI
import CoreLocation
import MapKit
import SafariServices

/**
 This section is reponsible for displaying Address information such as,
 * School Name and Borough
 * Address -> Opens Direction on Map App
 * Email -> Opens Email Composer
 * PhoneNumber -> Opens Call app and make a call
 * Website -> Open the School site inside the App using SFSafariServices
 */

struct AddressSection: SectionDetails {
    var title: String = ""
    var cells: [CellType] = []
    var school: School?
    
    init(school: School) {
        self.title = Constants.info
        self.school = school
        self.cells = [CellType.subTitle(title: school.school_name ?? "", subTitle: school.borough ?? "")]
        if self.getCompleteAddressWithoutCoordinate() != "" {
            self.cells.append(CellType.infoLink(image: Constants.locationImg, text: self.getCompleteAddressWithoutCoordinate(), type: .direction))
        }
        if let email = school.school_email,  email != "" {
            self.cells.append(CellType.infoLink(image: Constants.emailImg, text: email, type: .email))
        }
        if let phone = school.phone_number,  phone != "" {
            self.cells.append(CellType.infoLink(image: Constants.phoneImg, text: phone, type: .phoneNumber))
        }
        if let web = school.website,  web != "" {
            self.cells.append(CellType.infoLink(image: Constants.linkImg, text: web, type: .website))
        }
    }
    
    func trigerAction(forIndex index: Int, viewController: UIViewController) {
        if case CellType.infoLink(_ , _, let type) = cells[index] {
            switch type{
            case .email:
                self.sendEmail(self.school?.school_email, viewController)
                break
            case .phoneNumber:
                self.makeAPhoneCall(self.formattedPhoneNumber())
                break
            case .website:
                self.showWebsite(self.formattedWebSite(), viewController)
                break
            case .direction:
                if let coordinates = getCoodinateForSchool() {
                    self.showDirection(latitude: coordinates.latitude, longitude: coordinates.longitude)
                }
            default: break
            }
        }
    }

}

extension AddressSection {
    
    /// - Returns: Stirng, address of the high school
    func getCompleteAddressWithoutCoordinate() -> String{
        if let schoolAddress = self.school?.location {
            let address = schoolAddress.components(separatedBy: "(")
            return address[0]
        }
        return ""
    }
    
    /// This function will fetch the coodinates for the selected High School location
    ///
    /// - Returns: CLLocationCoordinate2D, coodinate of High School location
    func getCoodinateForSchool() -> CLLocationCoordinate2D? {
        if let schoolAddress = self.school?.location {
            let coordinateString = schoolAddress.slice(from: "(", to: ")")
            let coordinates = coordinateString?.components(separatedBy: ",")
            if let coordinateArray = coordinates{
                let latitude = (coordinateArray[0] as NSString).doubleValue
                let longitude = (coordinateArray[1] as NSString).doubleValue
                return CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
            }
        }
        return nil
    }

    /// This function will format the websites to url format Which help loading on safari
    ///
    /// - Returns: URL in String type
    func formattedWebSite() -> String {
        guard let site = self.school?.website?.trimmingCharacters(in: .whitespacesAndNewlines) else { return "" }
        if site.hasPrefix("http") {
            return site
        } else {
            return "https://"+site
        }
    }
    
    /// This function will format  phone number to a format Which helps open via call app
    ///
    /// - Returns: PhoneNumber in String type
    func formattedPhoneNumber() -> String {
        guard let phoneNumber = self.school?.phone_number else { return "" }
        return phoneNumber.replacingOccurrences(of: "-", with: "")
    }
    
}

extension AddressSection {


    func sendEmail(_ email: String?, _ vc: UIViewController)  {
        guard let email = email else {
            return
        }
        // Show default mail composer
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = vc as? MFMailComposeViewControllerDelegate
            mail.setToRecipients([email])
            vc.present(mail, animated: true)
        
        // Show third party email composer if default Mail app is not present
        } else if let emailUrl = createEmailUrl(to: email) {
            UIApplication.shared.open(emailUrl)
        }
    }
    
    /// showWebsite will open the SFSafari inside our Application
    ///  Param: `Link` -> link to be opened on the internal safari
    ///  Param: `viewController` -> presentedViewController to be Passed
    /// - Returns: Void
    func showWebsite(_ link: String, _ vc: UIViewController) {
        if let url = URL(string: link) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true

            let sf = SFSafariViewController(url: url, configuration: config)
            vc.present(sf, animated: true)
        }
    }

    /// Allows us to initiate Call flow
    ///  Param: `Phone` -> number to be dialed upon
    /// - Returns: Void
    func makeAPhoneCall(_ phone: String)  {
        if let phoneCallURL = URL(string: "tel:\(phone)") {
            UIApplication.shared.open(phoneCallURL, options: [:], completionHandler: nil)
        }
    }
    
    /// Open Maps app with the location
    ///  Param: `latitude & longitude` -> Schools lat and lon coordinates to be passed
    /// - Returns: Void
    func showDirection(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        // Open and show coordinate
        let url = "http://maps.apple.com/maps?saddr=\(latitude),\(longitude)"
        if let url = URL(string:url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }

    }

    /// Helper to send external mail composer other than Mail app
    ///  Param: `to` -> email address of the school to be passed
    /// - Returns: URL -> of external mail composer app
    private func createEmailUrl(to: String) -> URL? {
        let gmailUrl = URL(string: "googlegmail://co?to=\(to)")
        let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)")
        let yahooMail = URL(string: "ymail://mail/compose?to=\(to)")
        let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(to)")
        let defaultUrl = URL(string: "mailto:\(to)")
        
        if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
            return gmailUrl
        } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
            return outlookUrl
        } else if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {
            return yahooMail
        } else if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {
            return sparkUrl
        }
        return defaultUrl
    }

}


