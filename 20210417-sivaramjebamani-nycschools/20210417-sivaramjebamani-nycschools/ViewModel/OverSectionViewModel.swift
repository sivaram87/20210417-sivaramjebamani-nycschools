//
//  OverSectionViewModel.swift
//  20210417-sivaramjebamani-nycschools
//
//  Created by Sivaram jebamani on 4/17/21.
//
import UIKit

/**
 This section is reponsible for displaying Overview information about the school
 */

struct OverviewSection: SectionDetails {
    var title: String = ""
    var cells: [CellType] = []
    init(school: School) {
        self.title = Constants.overview
        self.cells = [
            CellType.text(text: school.overview_paragraph ?? ""),
                ]
    }
    func trigerAction(forIndex index: Int, viewController: UIViewController) {}
}
