//
//  SectionDetailsProtocol.swift
//  20210417-sivaramjebamani-nycschools
//
//  Created by Sivaram jebamani on 4/17/21.
//

import UIKit

/// SectionDetails Protocol makes the section to be easily customizable
///     `title` -> Section Header title to be passed
///     `cells` -> Each rows to be defined as cellType
///     `trigerAction(:)` -> Helps to handle each rows action

protocol SectionDetails {
    var title: String { get set }
    var cells: [CellType] { get set }
    func trigerAction(forIndex index: Int, viewController: UIViewController)
}

