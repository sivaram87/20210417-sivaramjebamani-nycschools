//
//  CellType.swift
//  20210417-sivaramjebamani-nycschools
//
//  Created by Sivaram jebamani on 4/17/21.
//

import UIKit

/// CellType enum helps to identify the cell design to be displayed.
/// Each Cell Type has the associated property to hold is display value

enum CellType {
    case text(text: String)
    case infoLink(image: String, text: String, type: LinkType)
    case rightSubTitle(title: String, subTitle: String)
    case subTitle(title: String, subTitle: String)
}

enum LinkType {
    case email
    case phoneNumber
    case website
    case direction
    case other(Any)
}

extension CellType {
    private func cellInfoLink(tableView: UITableView, atIndexPath indexPath: IndexPath) -> NYSInfoLinkCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.infoListCell, for: indexPath) as? NYSInfoLinkCell else {
            return nil
        }
        return cell
    }

    private func cellRightSubTitle(tableView: UITableView, atIndexPath indexPath: IndexPath) -> NYSRightInfoCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.rightInfoCell, for: indexPath) as? NYSRightInfoCell else {
            return nil
        }
        return cell
    }

    
    private func cellSubTitle(tableView: UITableView, atIndexPath indexPath: IndexPath) -> NYSDefaultCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.defaultCell, for: indexPath) as? NYSDefaultCell else {
            return nil
        }
        return cell
    }
    
    private func cellText(tableView: UITableView, atIndexPath indexPath: IndexPath) -> NYSTextCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.textCell, for: indexPath) as? NYSTextCell else {
            return nil
        }
        return cell
    }

}

extension CellType {
    func getCell(tableView: UITableView, atIndexPath indexPath: IndexPath) -> UITableViewCell {
        switch self {
        case .text(let text):
            if let cell = self.cellText(tableView: tableView, atIndexPath: indexPath) {
                cell.setText(text: text)
                return cell
            }
        case .infoLink(let image, let text, let type):
            if let cell = self.cellInfoLink(tableView: tableView, atIndexPath: indexPath) {
                cell.setUI(text: text, image: image, type: type)
                return cell
            }
        case .rightSubTitle(let title, let subTitle):
            if let cell = self.cellRightSubTitle(tableView: tableView, atIndexPath: indexPath) {
                cell.setUI(title: title, subTitle: subTitle)
                return cell
            }
        case .subTitle(let title, let subTitle):
            if let cell = self.cellSubTitle(tableView: tableView, atIndexPath: indexPath) {
                cell.setUI(title: title, subTitle: subTitle)
                return cell
            }
        }
        return UITableViewCell()
    }
}
