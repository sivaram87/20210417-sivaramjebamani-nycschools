//
//  NYSTextCell.swift
//  20210417-sivaramjebamani-nycschools
//
//  Created by Sivaram jebamani on 4/17/21.
//

import UIKit

class NYSTextCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.titleLabel.font = UIFont.preferredFont(forTextStyle: .callout)
    }

    func setText(text: String) {
        self.titleLabel.text = text
    }
}
