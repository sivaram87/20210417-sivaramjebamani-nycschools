//
//  NYSSchoolListCell.swift
//  20210417-sivaramjebamani-nycschools
//
//  Created by Sivaram jebamani on 4/18/21.
//

import UIKit

class NYSSchoolListCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.titleLabel.font = UIFont.preferredFont(forTextStyle: .headline)
        self.subTitleLabel.font = UIFont.preferredFont(forTextStyle: .subheadline)
    }

}
