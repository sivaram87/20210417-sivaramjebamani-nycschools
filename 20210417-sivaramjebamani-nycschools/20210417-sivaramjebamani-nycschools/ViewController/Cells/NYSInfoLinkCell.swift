//
//  NYSInfoLinkCell.swift
//  20210417-sivaramjebamani-nycschools
//
//  Created by Sivaram jebamani on 4/17/21.
//

import UIKit

class NYSInfoLinkCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setUI(text: String, image: String, type: LinkType) {
        self.titleLabel.text = text
        self.imgView.image = UIImage(systemName: image)
    }
}
