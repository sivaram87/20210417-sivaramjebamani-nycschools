//
//  NYSDetailsTableVC.swift
//  20210417-sivaramjebamani-nycschools
//
//  Created by Sivaram jebamani on 4/17/21.
//

import UIKit
import MessageUI

class NYSDetailsTableVC: UITableViewController {

    var school: School?
    var satResult: SATResult?
    
    var sections: [SectionDetails] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Constants.detailsTitle
        
        if let school = school {
            /// Setup the sections with all available SectionDetails implemented structs
            
            sections.append(AddressSection(school: school))
            if let satResult = satResult {
                sections.append(SATSection(satResult: satResult))
            }
            sections.append(OverviewSection(school: school))
            sections.append(TransportationSection(school: school))
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return sections[section].cells.count
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].title
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return sections[indexPath.section].cells[indexPath.row].getCell(tableView: tableView, atIndexPath: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        sections[indexPath.section].trigerAction(forIndex: indexPath.row, viewController: self)
    }
}

extension NYSDetailsTableVC: MFMailComposeViewControllerDelegate {
    
    /// If MailComposer opened then this delegate will be called while finishing the operation on the MailComposer Modal
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }

}
