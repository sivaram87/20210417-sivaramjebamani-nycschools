//
//  NYSHomeTableVC.swift
//  20210417-sivaramjebamani-nycschools
//
//  Created by Sivaram jebamani on 4/17/21.
//

import UIKit

class NYSHomeTableVC: UITableViewController {

    @IBOutlet weak var errView: UIView!
    @IBOutlet weak var errLabel: UILabel!
    @IBOutlet weak var reloadBtn: UIButton!
    
    let dataManager = SchoolDataManager()
    var schoolList: [School]?

    let search = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Constants.appTitle
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.setUpSearchController()
        self.loadSchoolList()
    }

    func setUpSearchController() {
        search.searchBar.delegate = self
        search.obscuresBackgroundDuringPresentation = false
        search.searchBar.placeholder = Constants.searchPlaceholder
        navigationItem.searchController = search
        navigationItem.hidesSearchBarWhenScrolling = false
    }

    /**
     Load the school data for the server and will update the view with Data/Error
     - Returns: Void
     */

    func loadSchoolList() {
        self.errView.isHidden = true
        self.loadActivityIndicator()
        self.dataManager.getAllHighSchools({[weak self] (list, error) in
            self?.schoolList = list
            DispatchQueue.main.async {
                self?.dismiss(animated: true, completion: nil)
                self?.updateErrorMessage(error: error)
                self?.tableView.reloadData()
            }
        })
    }

    /**
     Handles the error and update the error Label
     - Parameters: error -> NYSError
     - Returns: Void
     */
    func updateErrorMessage(error: NYSError?) {
        defer {
            let frame = self.errView.frame
            let count = self.schoolList?.count ?? 0
            self.errView.frame = CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.size.width, height: (count != 0) ? 0 : self.view.frame.height)
            self.errView.isHidden = (count != 0)
        }
        guard let err = error else {
            self.errLabel.text = Constants.noDataFound
            return
        }
        switch err {
        case .invalidData:
            self.errLabel.text = Constants.noDataFound
        case .invalidUrl:
            self.errLabel.text = Constants.somethingWrong
        case .networkFailed(_, _):
            self.errLabel.text = Constants.noConnection
        }
    }

    @IBAction func actionReload(_ sender: Any) {
        self.loadSchoolList()
    }
}

extension NYSHomeTableVC {
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolList?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.schoolListCell) as? NYSSchoolListCell else {
            return UITableViewCell()
        }
        let school = self.schoolList?[indexPath.row]
        cell.titleLabel?.text = school?.school_name
        cell.subTitleLabel?.text = school?.borough
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.navigateToDetailsView(indexPath)
    }
}


extension NYSHomeTableVC {
    /**
     Handles the Navigation to Details View by making service call to fetch SATResult and pass it the DetailsVC
     - Parameters: `IndexPath` -> Selected indexPath of the cell to be passed
     - Returns: Void
     */
    func navigateToDetailsView(_ indexPath: IndexPath) {
        if let selectedSchool = self.schoolList?[indexPath.row], let dbn = selectedSchool.dbn {
            self.dataManager.getSATInfo(for: dbn) { result, error in
                DispatchQueue.main.async {
                    if let detailVC = UIStoryboard(name: ViewIdentifiers.storyboard, bundle: .main).instantiateViewController(identifier: ViewIdentifiers.detailView) as? NYSDetailsTableVC {
                        detailVC.school = selectedSchool
                        detailVC.satResult = result
                        self.navigationController?.pushViewController(detailVC, animated: true)
                    }
                }
            }
        }
    }
}

extension NYSHomeTableVC: UISearchBarDelegate {
    /**
     Handles search button clicked action
      Check if the searchBar text is Valid
        If `Yes` then filter from the original dataManager List
            If `No` then prompt the validation error
     - Parameters: `UISearchBar` -> It is delegate function of search bar
     - Returns: Void
     */
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else { return }
        self.dismiss(animated: true) {
            if !self.validateSpaces(text: text) { return }
            self.schoolList = self.dataManager.schools?.filter({
                if let name = $0.school_name {
                    return name.contains(text)
                }
                return false
            })
            self.tableView.reloadData()
        }
    }
    
    /**
     Handles search Cancel button click action
      Reload the tableView with the original data set
     */
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.schoolList = self.dataManager.schools
        self.tableView.reloadData()
    }
    
    /**
     Validate empty spaces and newLine
     - Parameters: text -> String
     - Returns: Bool -> return true if passed; false if failed
     */
    func validateSpaces(text: String) -> Bool {
        if text.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            let alert = UIAlertController(title: Constants.error, message: Constants.errorValidMsg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: Constants.errorOk, style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        return true
    }
}
