//
//  School.swift
//  20210417-sivaramjebamani-nycschools
//
//  Created by Sivaram jebamani on 4/17/21.
//

import Foundation

struct School: Codable {
    var dbn: String?
    var school_name: String?
    var overview_paragraph: String?
    var location: String?
    var phone_number: String?
    var school_email: String?
    var website: String?
    var subway: String?
    var bus: String?
    var borough: String?
}
