//
//  NYSError.swift
//  20210417-sivaramjebamani-nycschools
//
//  Created by Sivaram jebamani on 4/17/21.
//

/**
    Error enum to handle all the failures happened during the network connection
 */

enum NYSError: Error {
    case invalidUrl
    case networkFailed(code: Int, description: String)
    case invalidData
}

