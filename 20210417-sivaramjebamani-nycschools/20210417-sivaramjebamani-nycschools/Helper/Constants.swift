//
//  Constants.swift
//  20210417-sivaramjebamani-nycschools
//
//  Created by Sivaram jebamani on 4/17/21.
//


struct NYSConfig {
    static let SCHOOL_API_ENDPOINT = "s3k6-pzi2.json"
    static let SAT_API_ENDPOINT = "f9bf-2cp4.json"
    
    static let API_URL = "https://data.cityofnewyork.us/resource/"
    static let APPTOKEN = "xYjnIfN0z2BJRZh0YVMa9lECR"
    
    static let APPTOKEN_KEY = "$$app_token"
}

struct Constants {
    static let appTitle = "NYC Schools"
    static let searchPlaceholder = "Search by school name"
    static let detailsTitle = "School Details"
    
    static let info = "Information"
    static let satResults = "SAT Results"
    static let overview = "Overview"
    static let transportation = "Transportation"
    static let bus = "Bus"
    static let subway = "Subway"
    static let testTaker = "Test Takers"
    static let mathScore = "Math Score"
    static let writingScore = "Writing Score"
    static let readingScore = "Critical Reading Score"
    static let na = "N/A"
    
    // Image Assets
    
    static let locationImg = "location.circle.fill"
    static let emailImg = "envelope.circle.fill"
    static let phoneImg = "phone.circle.fill"
    static let linkImg = "link.circle.fill"

    
    
    static let noDataFound = "No Data Found."
    static let noConnection = "Check your internet connection."
    static let somethingWrong = "Something went wrong."

    static let error = "Error"
    static let errorValidMsg = "Please enter valid input."
    static let errorOk = "Ok"
}

struct CellIdentifiers {
    static let schoolListCell = "schoolListCell"
    
    static let infoListCell = "NYSInfoLinkCell"
    static let rightInfoCell = "NYSRightInfoCell"
    static let defaultCell = "NYSDefaultCell"
    static let textCell = "NYSTextCell"
}

struct ViewIdentifiers {
    static let storyboard = "Main"
    static let detailView = "DetailView"
}
