//
//  SchoolDataManager.swift
//  20210417-sivaramjebamani-nycschools
//
//  Created by Sivaram jebamani on 4/17/21.
//

import Foundation

typealias SchoolDataHandler = (_ schools: [School]?,_ error: NYSError?) -> Void
typealias SATDataHandler = (_ result: SATResult?,_ error: NYSError?) -> Void
typealias NetworkHandler = (_ data: Data?, _ error: NYSError?) -> Void

class SchoolDataManager {

    var schools: [School]?

    /**
     This function helps is fetching all the schools data from the NYS HighSchool Directory
     - Parameters: `SchoolDataHandler` -> will return as a function with arguments such as `[School]` & `NYSError`
     - Returns: Void
     */
    
    func getAllHighSchools(_ handler: @escaping SchoolDataHandler){
        self.makeAPICall(NYSConfig.API_URL + NYSConfig.SCHOOL_API_ENDPOINT) { data, error in
            guard let data = data else {
                handler(nil, error)
                return
            }
            if let schoolList = try? JSONDecoder().decode([School].self, from: data) {
                self.schools = schoolList
                handler(schoolList, nil)
            } else {
                handler(nil, NYSError.invalidData)
            }
        }
    }
    
    /**
     This function helps is fetching SAT data for particular dbn
     - Parameters: `SATDataHandler` -> will return as a function with arguments such as `SATResult` & `NYSError`
     - Returns: Void
     */
    
    func getSATInfo(for dbn: String, _ handler: @escaping SATDataHandler) {
        self.makeAPICall(NYSConfig.API_URL + NYSConfig.SAT_API_ENDPOINT,
                         ["dbn": dbn]) { data, error in
            guard let data = data else {
                handler(nil, error)
                return
            }
            if let satResult = try? JSONDecoder().decode([SATResult].self, from: data) {
                handler(satResult.first, nil)
            } else {
                handler(nil, NYSError.invalidData)
            }
        }
    }
    
    /**
     This function helps making the Network call to the server by passing the below parameters
     - Parameters: `URL` -> URL which data is available
     - Parameters: `params` -> Params to be passed as a ?key=Value using QueryItem
     - Parameters: `handler` -> will return as a function with arguments such as Data & error
     - Returns: Void
     */

    func makeAPICall(_ url: String, _ params: [String: String] = [:], _ handler: @escaping NetworkHandler) {
        guard var component = URLComponents(string: url) else {
            handler(nil, NYSError.invalidUrl)
            return
        }
        var queryItems = [URLQueryItem(name: NYSConfig.APPTOKEN_KEY, value: NYSConfig.APPTOKEN)]
        for (key, value) in params {
            queryItems.append(URLQueryItem(name: key, value: value))
        }
        component.queryItems = queryItems
        let session = URLSession.shared
        let request = URLRequest(url: component.url!)
        let task = session.dataTask(with: request,
                                    completionHandler: { data, response, networkError in
                                        if let _ = networkError {
                                            handler(nil, NYSError.networkFailed(code: (response as? HTTPURLResponse)?.statusCode ?? 400, description: networkError.debugDescription))
                                            return
                                        }
                                        
                                        guard let data = data else {
                                            handler(nil, NYSError.invalidData)
                                            return
                                        }
                                        handler(data, nil)
        })
        task.resume()
    }
}
